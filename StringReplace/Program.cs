﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace StringReplace
{
    internal class Program
    {
        private static void Pause()
        {
            Console.WriteLine();
            Console.WriteLine("Press Enter key to exit...");
            Console.ReadLine();
        }

        private static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Require min 3 args: output file name, input string, variants[]");
                Pause();
            }
            else
            {
                try
                {
                    var fileName = args[0];
                    var inputString = args[1];
                    var stringBuilder = new StringBuilder();
                    for (int i = 2; i < args.Length; i++)
                    {
                        stringBuilder.AppendLine(inputString.Replace("%X%", args[i]));
                    }

                    File.AppendAllText(fileName, stringBuilder.ToString());
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                    Pause();
                }
            }
        }
    }
}
﻿namespace StringReplaceApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rtbInput = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rtbVariants = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.btnReplace = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Текст шаблон";
            // 
            // rtbInput
            // 
            this.rtbInput.Location = new System.Drawing.Point(13, 30);
            this.rtbInput.Name = "rtbInput";
            this.rtbInput.Size = new System.Drawing.Size(452, 168);
            this.rtbInput.TabIndex = 1;
            this.rtbInput.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(500, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(247, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Значения переменной (каждое с новой строки)";
            // 
            // rtbVariants
            // 
            this.rtbVariants.Location = new System.Drawing.Point(503, 30);
            this.rtbVariants.Name = "rtbVariants";
            this.rtbVariants.Size = new System.Drawing.Size(452, 168);
            this.rtbVariants.TabIndex = 2;
            this.rtbVariants.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 211);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Результат";
            // 
            // rtbOutput
            // 
            this.rtbOutput.Location = new System.Drawing.Point(13, 227);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.Size = new System.Drawing.Size(952, 347);
            this.rtbOutput.TabIndex = 3;
            this.rtbOutput.Text = "";
            // 
            // btnReplace
            // 
            this.btnReplace.Location = new System.Drawing.Point(876, 580);
            this.btnReplace.Name = "btnReplace";
            this.btnReplace.Size = new System.Drawing.Size(89, 23);
            this.btnReplace.TabIndex = 4;
            this.btnReplace.Text = "Генерировать";
            this.btnReplace.UseVisualStyleBackColor = true;
            this.btnReplace.Click += new System.EventHandler(this.btnReplace_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 615);
            this.Controls.Add(this.btnReplace);
            this.Controls.Add(this.rtbOutput);
            this.Controls.Add(this.rtbVariants);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rtbInput);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(993, 654);
            this.MinimumSize = new System.Drawing.Size(993, 654);
            this.Name = "MainForm";
            this.Text = "Генератор строк";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox rtbInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox rtbVariants;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox rtbOutput;
        private System.Windows.Forms.Button btnReplace;
    }
}


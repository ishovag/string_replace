﻿using System;
using System.Text;
using System.Windows.Forms;
using StringReplaceApp.Properties;

namespace StringReplaceApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Text = Application.ProductName;
            LoadConfig();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveConfig();
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            rtbOutput.Clear();
            var inputText = rtbInput.Text;
            var stringBuilder = new StringBuilder();
            foreach (var variant in rtbVariants.Text.Split(new[] {"\n"}, StringSplitOptions.RemoveEmptyEntries))
            {
                stringBuilder.AppendLine(inputText.Replace("%X%", variant));
            }

            rtbOutput.Text = stringBuilder.ToString();
        }

        private void LoadConfig()
        {
            rtbInput.Text = Settings.Default.InputText;
            rtbVariants.Text = Settings.Default.Variants;
        }

        private void SaveConfig()
        {
            try
            {
                Settings.Default.InputText = rtbInput.Text;
                Settings.Default.Variants = rtbVariants.Text;
                Settings.Default.Save();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}